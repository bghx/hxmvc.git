package com.bghx.bghxmvc.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bghx.bghxmvc.form.ActionForm;

public interface Action {
	String execute(HttpServletRequest request,HttpServletResponse response,ActionForm form,Map<String,String> forwardMap);
}
