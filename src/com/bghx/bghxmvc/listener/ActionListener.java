package com.bghx.bghxmvc.listener;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.bghx.bghxmvc.util.BghxMvcConfig;
import com.bghx.bghxmvc.vo.XmlBean;



public class ActionListener  implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {

		System.out.println("系统已停止");
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		ServletContext context = arg0.getServletContext();
		String xmlPath = context.getInitParameter("bghxmvc-config");
		String tomcatPath = context.getRealPath("\\");
		try{
			Map<String, XmlBean> map = BghxMvcConfig.getXml(tomcatPath+xmlPath);
			context.setAttribute("bghxmvc-config", map);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("系统已经加载完成");
		
	}

}
