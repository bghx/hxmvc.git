package com.bghx.bghxmvc.form;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

public class FillForm {
	
	public static ActionForm fill(HttpServletRequest request, String className){
		ActionForm form = null;
		try {
			Class clazz = Class.forName(className);
			form = (ActionForm) clazz.newInstance();
			Field[] fields = clazz.getDeclaredFields();
			for(Field field:fields){
				field.setAccessible(true);
				field.set(form, request.getParameter(field.getName()));
				field.setAccessible(false);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return form;
		
	}
}
