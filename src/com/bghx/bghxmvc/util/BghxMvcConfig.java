package com.bghx.bghxmvc.util;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bghx.bghxmvc.vo.XmlBean;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

public class BghxMvcConfig {
	public static Map<String, XmlBean> getXml(String xmlPath) {
		Map<String, XmlBean> xmlMap = new HashMap<String, XmlBean>();
		try {
			SAXBuilder builder = new SAXBuilder();
			Document document = builder.build(new File(xmlPath));
			Element rootElement = document.getRootElement();
			Element actionform = rootElement.getChild("formbeans");
			Element actionMapping = rootElement.getChild("action-mapping");
			List<Element> actions = actionMapping.getChildren();
			for(Element action:actions){
				XmlBean xmlBean = new XmlBean();
				String path = action.getAttributeValue("path");
				List<Element> beans = actionform.getChildren();
				String beanName = action.getAttributeValue("name");
				xmlBean.setBeanName(beanName);
				for(Element bean:beans){
					if(beanName.equals(bean.getAttributeValue("name"))){
						xmlBean.setFormClass(bean.getAttributeValue("class"));
						break;
					}
				}
				xmlBean.setActionClass(action.getAttributeValue("type"));
				xmlBean.setPath(path);
				
				List<Element> forwards = action.getChildren();
				Map<String, String> fMap = new HashMap<String,String>();
				for(Element forward:forwards){
					fMap.put(forward.getAttributeValue("name"), forward.getAttributeValue("value"));
				}
				xmlBean.setForwardMap(fMap);
				
				xmlMap.put(path, xmlBean);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return xmlMap;
	}
}
