package com.bghx.bussiness.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bghx.bghxmvc.action.Action;
import com.bghx.bghxmvc.form.ActionForm;
import com.bghx.bussiness.form.LoginForm;
import com.bghx.bussiness.service.UserService;
import com.bghx.bussiness.service.impl.UserServiceImpl;
import com.bghx.bussiness.vo.UserInfo;

public class LoginAction implements Action {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionForm form,
			Map<String, String> forwardMap) {
		LoginForm myFrom=(LoginForm)form;
		if (myFrom.getUsername().equals("bghx")) {
			
			UserService userService=new UserServiceImpl();
			
			UserInfo userInfo=userService.getUserInfo();
			
			request.setAttribute("userInfo", userInfo);
			
			return forwardMap.get("success");			
		}else {
			return forwardMap.get("fail");		
		}
	}

}
