package com.bghx.bussiness.service.impl;

import com.bghx.bussiness.service.UserService;
import com.bghx.bussiness.vo.UserInfo;

public class UserServiceImpl implements UserService {

	@Override
	public UserInfo getUserInfo() {
		UserInfo userInfo=new UserInfo();
		userInfo.setUsername("bghx");
		userInfo.setAge(10);
		return userInfo;
	}

}
